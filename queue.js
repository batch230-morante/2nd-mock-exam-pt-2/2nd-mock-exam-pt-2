
let collection = [];
// Write the queue functions below.
// (1) Make sure to have correct function names
// 1. 
function print(){
    return collection;
}
// -----------------------------------------------------
// 2. + 3.
function enqueue(element){
    collection[collection.length]= element;
    return collection;

}
// -----------------------------------------------------
// 4. + 5. + 6.
function dequeue(){
    for(let i=0; i<=collection.length; i++){ 
        collection[i] = collection[i + 1];
    }
    collection.length--;
    return collection;
}
// -----------------------------------------------------
// 7.
function front(){
    return collection[0];
}
// -----------------------------------------------------
// 8.

function size(){
    return collection.length;
}
// ----------------------------------------------------
// 9.
function isEmpty(collection){
    if(collection == true){
        return true;
    }
    else{
        return false;
    }
}
// (2) Make sure to include the functions in exportation
module.exports = {
print, enqueue, dequeue, front, size, isEmpty
};
